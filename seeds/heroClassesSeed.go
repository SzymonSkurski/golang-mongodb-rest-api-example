package seeds

import "go.mongodb.org/mongo-driver/bson"

func HeroClassesSeed() []interface{} {
	return []interface{}{
		bson.D{
			{"Name", "tank"},
			{"attack", 5},
			{"defence", 30},
			{"pierce", 5},
			{"HP", 60},
		},
		bson.D{
			{"Name", "protector"},
			{"attack", 5},
			{"defence", 60},
			{"pierce", 5},
			{"HP", 30},
		},
	}
}
