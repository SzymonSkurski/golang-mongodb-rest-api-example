package seeds

import "go.mongodb.org/mongo-driver/bson"

func HeroTemplatesSeed() []interface{} {
	return []interface{}{
		bson.D{
			{"Name", "Tank 1"},
			{"class", "tank"},
			{"abilities", bson.A{"stone skin"}},
		},
		bson.D{
			{"Name", "Protector 1"},
			{"class", "protector"},
			{"abilities", bson.A{"iron skin"}},
		},
	}
}
