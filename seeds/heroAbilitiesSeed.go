package seeds

import (
	"go.mongodb.org/mongo-driver/bson"
)

func HeroAbilitiesSeed() []interface{} {
	return []interface{}{
		bson.D{
			{"Name", "stone skin"},
			{"kind", "passive"},
			{"power", 50},
			{"powerPerLvl", 10},
		},
		bson.D{
			{"Name", "iron skin"},
			{"kind", "passive"},
			{"power", 100},
			{"powerPerLvl", 25},
		},
	}
}
