package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
)

type MongoTimeLimits struct {
	Insert     time.Duration // how much time for insert one document
	InsertMany time.Duration // how much time for insert multiple documents
	FindOne    time.Duration // how much time for find one document
	FindMany   time.Duration // how much time for find multiple documents
	Delete     time.Duration // how much time for delete single document
	Iterate    time.Duration // how much time for iterate trough collection
	Count      time.Duration // how much time for count collection
	Update     time.Duration // how much time for update single document
}

func NewMongoTimeLimits() *MongoTimeLimits {
	return &MongoTimeLimits{
		Insert:     2 * time.Second,
		InsertMany: 5 * time.Second,
		FindOne:    1 * time.Second,
		FindMany:   3 * time.Second,
		Delete:     3 * time.Second,
		Iterate:    5 * time.Second,
		Count:      1 * time.Second,
		Update:     1 * time.Second,
	}
}

type MongoStorage struct {
	// in the case of multiple connections, names provide easy identification
	Name        string
	uri         string
	client      *mongo.Client
	ctx         context.Context
	cancelFunc  func()
	databases   map[string]*mongo.Database
	collections map[string]map[string]*mongo.Collection
	Limits      *MongoTimeLimits
}

// NewMongoStorage create new MongoDB connection client
func NewMongoStorage(uri string, ctx context.Context, cancelFunc func()) *MongoStorage {
	ms := &MongoStorage{
		uri:         uri,
		client:      nil,
		ctx:         ctx,
		cancelFunc:  cancelFunc,
		databases:   map[string]*mongo.Database{},              // init map
		collections: map[string]map[string]*mongo.Collection{}, // init map
		Limits:      NewMongoTimeLimits(),
	}

	ms.MustConnect()

	return ms
}

func (ms *MongoStorage) MustConnect() {
	if ms.client != nil {
		return
	}
	// "mongodb://<User>:<password>@<Host>:<Port>/" default Port=27017
	// "mongodb://root:example@localhost:27017" for example
	uri := ms.uri
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI(uri).SetServerAPIOptions(serverAPI)

	client, err := mongo.Connect(ms.ctx, opts)
	if err != nil {
		log.Fatal(err)
	}
	ms.client = client

	if err := ms.Ping(); err != nil {
		log.Fatal(err)
	}
	log.Println("MongoDB connected")
}

func (ms *MongoStorage) Ping() error {
	if ms.client == nil {
		return fmt.Errorf("not connected")
	}

	return ms.client.Ping(ms.ctx, readpref.Primary())
}

func (ms *MongoStorage) Disconnect() {
	ms.cancelFunc()
	if err := ms.client.Disconnect(ms.ctx); err != nil {
		log.Println(err)
	}
}

func (ms *MongoStorage) ConnectDB(DBName string) *mongo.Database {
	if DB, ok := ms.databases[DBName]; ok {
		return DB
	}
	// will create database after create first collection in it
	ms.databases[DBName] = ms.client.Database(DBName)

	return ms.databases[DBName]
}

func (ms *MongoStorage) GetCollection(DBName, collectionName string) *mongo.Collection {
	if collection, ok := ms.collections[DBName][collectionName]; ok {
		return collection
	}
	if _, ok := ms.collections[DBName]; !ok {
		ms.collections[DBName] = map[string]*mongo.Collection{}
	}
	ms.collections[DBName][collectionName] = ms.ConnectDB(DBName).Collection(collectionName)

	return ms.collections[DBName][collectionName]
}

// Count will count collection documents according to given options (default count all by ID)
// SetCollation() 	The type of language collation to use when sorting results. 		Default: nil
// SetHint() 		The index to use to scan for documents to count. 					Default: nil
// SetLimit() 		The maximum number of documents to count. 							Default: 0
// SetMaxTime() 	The maximum amount of time that the query can run on the server. 	Default: nil
// SetSkip() 		The number of documents to skip before counting. 					Default: 0
//
// example, count all documents, Count("templates", "heroAbilities", nil)
// read more: https://www.mongodb.com/docs/drivers/go/current/fundamentals/crud/read-operations/count/
func (ms *MongoStorage) Count(DBName, collectionName string, opts *options.CountOptions, filter interface{}) (int64, error) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.InsertMany)

	defer cancel()

	if opts == nil {
		opts = options.Count().SetHint("_id_")
	}

	if filter == nil {
		filter = bson.M{}
	}

	collection := ms.GetCollection(DBName, collectionName)

	return collection.CountDocuments(ctx, filter, opts)
}

func (ms *MongoStorage) Seed(DBName, collectionName string, data []interface{}) (*mongo.InsertManyResult, error) {
	if c, _ := ms.Count(DBName, collectionName, nil, nil); c > 0 {
		return &mongo.InsertManyResult{}, fmt.Errorf("%s:%s collection is not empty", DBName, collectionName) // collection is not empty
	}

	return ms.InsertMany(DBName, collectionName, data)
}

func (ms *MongoStorage) InsertMany(DBName, collectionName string, data []interface{}) (*mongo.InsertManyResult, error) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.InsertMany)

	defer cancel()

	collection := ms.GetCollection(DBName, collectionName)

	return collection.InsertMany(ctx, data)
}

func (ms *MongoStorage) FindAll(DBName, collectionName string, result interface{}) interface{} {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.FindMany)

	defer cancel()

	collection := ms.GetCollection(DBName, collectionName)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	if result == nil {
		result = []bson.M{}
	}

	if err = cursor.All(ms.ctx, &result); err != nil {
		log.Fatal(err)
	}
	return result
}

// IterateCollection will iterate through ech collection document and run function on it
func (ms *MongoStorage) IterateCollection(DBName, collectionName string, closure func(document *bson.M)) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.Iterate)

	defer cancel()

	collection := ms.GetCollection(DBName, collectionName)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		_ = cursor.Close(ctx)
	}()

	for cursor.Next(ctx) {
		doc := &bson.M{}
		if err := cursor.Decode(doc); err != nil {
			log.Fatal(err)
		}
		closure(doc)
	}
}

func (ms *MongoStorage) FindOne(DBName, collectionName string, filter interface{}) (bson.M, error) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.InsertMany)

	defer cancel()

	result := bson.M{}

	collection := ms.GetCollection(DBName, collectionName)
	err := collection.FindOne(ctx, filter).Decode(&result)

	return result, err
}

func (ms *MongoStorage) FindMultiple(DBName, collectionName string, filter interface{}, order interface{}, result interface{}, pagination *Pagination) (interface{}, error) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.FindMany)

	defer cancel()

	collection := ms.GetCollection(DBName, collectionName)
	opts := options.Find().SetSkip(pagination.HowManyPagesToSkip()).SetLimit(pagination.PerPage)

	if order != nil {
		opts.SetSort(order) // bson.D{}
	}

	if result == nil {
		result = []bson.M{}
	}

	cursor, err := collection.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	if err = cursor.All(ms.ctx, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (ms *MongoStorage) DeleteByID(DBName, collectionName, hashID string) (*mongo.DeleteResult, error) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.FindMany)

	defer cancel()

	collection := ms.GetCollection(DBName, collectionName)
	// Declare a primitive ObjectID from a hexadecimal string
	idPrimitive, err := primitive.ObjectIDFromHex(hashID)
	if err != nil {
		return nil, err
	}
	// Call the DeleteOne() method by passing BSON map
	res, err := collection.DeleteOne(ctx, bson.M{"_id": idPrimitive})
	if err != nil {
		return nil, err
	}
	return res, nil
}

// UpdateOne updated one document identified by given ID
// set it could bson.M or entity struct
// example UpdateOne("library", "books", "64e1049c6b30457ad473565f", bson.M{"title":"Another book about Linux"})
// will change title in book._id:64e1049c6b30457ad473565f
func (ms *MongoStorage) UpdateOne(DBName, collectionName, ID string, set interface{}) (*mongo.UpdateResult, error) {
	ctx, cancel := context.WithTimeout(ms.ctx, ms.Limits.Update)

	defer cancel()

	collection := ms.GetCollection(DBName, collectionName)

	objID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return nil, err
	}

	return collection.UpdateOne(ctx, bson.D{{"_id", objID}}, bson.M{"$set": set})
}
