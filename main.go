package main

import (
	entities "authentication"
	"fmt"
	"github.com/gin-gonic/gin"
)

const DbTemplates = "templates"

func main() {
	// load configuration
	cfg := NewConfig()
	app := NewAPP(cfg)

	// seed database if empty
	seed(app.MongoDB)

	// run gin server
	serve(cfg, app)
}

func serve(cfg *Config, app *APP) {
	declareRoutes(app)
	// run gin server
	r := app.Engine
	host := cfg.Server.Host
	port := cfg.Server.Port
	_ = r.Run(fmt.Sprintf("%s:%d", host, port)) // listen and serve
}

func seed(mongoDB *MongoStorage) {
	_ = mongoDB.ConnectDB(DbTemplates) // will create database after create first collection
	// seeds data
	for _, entity := range entities.Entities() {
		r, err := mongoDB.Seed(DbTemplates, entity.ColName, entity.SeedFunc)
		if err != nil {
			fmt.Println(gin.H{entity.ColName: err.Error()})
		}
		fmt.Println(gin.H{entity.ColName: r.InsertedIDs})
	}
}

//r := mongoDB.FindAll(DbTemplates, entities.HeroTemplatesCol, []*entities.HeroTemplate{})
//heroTemplates := r.([]*entities.HeroTemplate) // cast result interface into entity
//prettyprinter.PP(heroTemplates)

//
//fmt.Println(templatesDB.ListCollectionNames(ctx, bson.M{}))
//
//// list all databases
//databases, err := mongoClient.ListDatabaseNames(ctx, bson.M{})
//if err != nil {
//	log.Fatal(err)
//}
//fmt.Println(databases)

//collection := templatesDB.Collection("heroAbilities")

//abilities := heroAbilitiesAll(heroAbilitiesColl, ctx)
//heroAbilities := all(DB_templates, "heroAbilities")
//prettyprinter.PP(heroAbilities)
//
//IterateCollection(DB_templates, "heroAbilities", func(document *bson.M) {
//	prettyprinter.PP(document)
//})

//stoneSkinAbility := FindOne(DB_templates, "heroAbilities", bson.M{"Name": "stone skin"})
//prettyprinter.PP(stoneSkinAbility)

////abilities with power in
//abilities := FindMultiple(DB_templates, "heroAbilities", bson.D{
//	{"power", bson.D{
//		{"$in", bson.A{50, 100}},
//	}},
//}, nil)
//prettyprinter.PP(abilities)
//
//// abilities with power greater than `gt` 50
//powerfulAbilities := FindMultiple(DB_templates, "heroAbilities", bson.D{
//	{"power", bson.D{
//		{"$gt", 50},
//	}},
//}, bson.D{{"power", -1}})
//prettyprinter.PP(powerfulAbilities)

//// abilities with power greater than or equal `gte` 50, order DESC by power
//powerfulAbilities2 := FindMultiple(DB_templates, "heroAbilities", bson.D{
//	{"power", bson.D{
//		{"$gte", 50},
//	}},
//}, bson.D{{"power", -1}})
//prettyprinter.PP(powerfulAbilities2)

//// regex
//// abilities with Name LIKE % skin, order DESC by power
//skinLikeAbilities := FindMultiple(DB_templates, "heroAbilities",
//	bson.M{"Name": bson.M{"$regex": "\\w+\\sskin$", "$options": ""}}, //bson.M{"permalink": bson.M{"$regex": "org.*", "$options": ""}}
//	bson.D{{"Name", 1}})
//prettyprinter.PP(skinLikeAbilities)

//DeleteByID(DB_templates, "heroAbilities", "64e31a153b5a9bf67fc78015")
//
//prettyprinter.PP(all(DB_templates, "heroAbilities"))
