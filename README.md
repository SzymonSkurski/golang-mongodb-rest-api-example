# GoLang MongoDB REST API example



## Getting started

Run `docker compose up` to start the MongoDB container and express.
In separate terminal run `go run .` to run web app

## Endpoints

### create document
```curl --location --request POST 'localhost:8085/heroTemplates?name=fire%20warior&OptionalName=fire%20monster&class=warior&abilities=1&abilities=2'```

### delete document
```curl --location --request DELETE 'localhost:8085/heroTemplates/6762e14605e193b8b22c0c34'```
