package main

import (
	entities "authentication"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type HeroTemplateRepository struct {
	MongoDB  *MongoStorage
	DBName   string
	CollName string
}

func NewHeroTemplateRepository(ms *MongoStorage) *HeroTemplateRepository {
	return &HeroTemplateRepository{
		MongoDB:  ms,
		DBName:   DbTemplates,
		CollName: entities.HeroTemplatesCol,
	}
}

func (rep *HeroTemplateRepository) All() []entities.HeroTemplate {
	r := rep.MongoDB.FindAll(rep.DBName, rep.CollName, []entities.HeroTemplate{})
	return r.([]entities.HeroTemplate)
}

func (rep *HeroTemplateRepository) Count(filter interface{}) int64 {
	c, _ := rep.MongoDB.Count(rep.DBName, rep.CollName, nil, filter)
	return c
}

func (rep *HeroTemplateRepository) Multiple(filter entities.HeroTemplate, order interface{}, pagination *Pagination) []entities.HeroTemplate {

	if r, err := rep.MongoDB.FindMultiple(rep.DBName, rep.CollName, filter, order, []entities.HeroTemplate{}, pagination); err == nil && r != nil {
		return r.([]entities.HeroTemplate)
	}

	return []entities.HeroTemplate{} // empty
}

func (rep *HeroTemplateRepository) FindOne(filter interface{}) (entities.HeroTemplate, error) {

	p := &Pagination{
		Page:    1,
		PerPage: 1,
	}
	r, err := rep.MongoDB.FindMultiple(rep.DBName, rep.CollName, filter, nil, []entities.HeroTemplate{}, p)
	if err != nil || len(r.([]entities.HeroTemplate)) == 0 {
		return entities.HeroTemplate{}, fmt.Errorf(fmt.Sprintf("%s:%s cannot get heroTemplate by: %v, reason: %v", rep.DBName, rep.CollName, filter, err))
	}

	return r.([]entities.HeroTemplate)[0], nil
}

func (rep *HeroTemplateRepository) FindOneByName(name string) (entities.HeroTemplate, error) {
	return rep.FindOne(bson.M{"name": name})
}

func (rep *HeroTemplateRepository) FindOneByID(ID string) (entities.HeroTemplate, error) {
	objID, err := primitive.ObjectIDFromHex(ID)
	if err != nil {
		return entities.HeroTemplate{}, fmt.Errorf("ID conversion error: %v", err)
	}

	return rep.FindOne(bson.M{"_id": objID})
}

func (rep *HeroTemplateRepository) Delete(ID string) (string, error) {
	r, err := rep.MongoDB.DeleteByID(rep.DBName, rep.CollName, ID)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s %d rows deleted", rep.CollName, r.DeletedCount), nil
}

func (rep *HeroTemplateRepository) UpdateOne(heroTemplate entities.HeroTemplate) (string, error) {
	if heroTemplate.ID.IsZero() {
		return "", fmt.Errorf("cannot update document with empty _id")
	}

	r, err := rep.MongoDB.UpdateOne(rep.DBName, rep.CollName, heroTemplate.ID.Hex(), heroTemplate)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s %d rows updated", rep.CollName, r.ModifiedCount), nil
}

func (rep *HeroTemplateRepository) CreateOne(heroTemplate entities.HeroTemplate) (string, error) {
	data := make([]interface{}, 1)
	data[0] = heroTemplate

	r, err := rep.MongoDB.InsertMany(rep.DBName, rep.CollName, data)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s new row _id:%v", rep.CollName, r.InsertedIDs[0].(primitive.ObjectID).Hex()), nil
}
