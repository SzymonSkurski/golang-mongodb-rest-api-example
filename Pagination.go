package main

import (
	"math"
	"strconv"
	"strings"
)

const PerPageDefault = 25 // PerPageDefault default value
const PageDefault = 1     // PageDefault default value

type Pagination struct {
	Page    int64 `json:"page"`
	PerPage int64 `json:"perPage"`
	Total   int64 `json:"total"`
}

// NewPagination will return reference to Pagination
// p string in form p:<page>,pp:<perPage> like p:1,pp:25"
// totalDocs how many documents inside the collection
func NewPagination(raw string, totalDocs int64) *Pagination {
	p := &Pagination{
		Page:    PageDefault,
		PerPage: PerPageDefault,
	}
	p.set(raw)
	p.Total = p.countTotalPages(totalDocs)
	return p
}

func (p *Pagination) set(raw string) {
	raws := strings.Split(raw, ",")
	for _, r := range raws {
		if rs := strings.Split(r, ":"); len(rs) > 1 {
			v, _ := strconv.ParseFloat(rs[1], 64)
			switch rs[0] {
			case "p":
				p.Page = int64(math.Max(float64(PageDefault), v))
				break
			case "pp":
				p.PerPage = int64(math.Max(1, v))
				break
			}
		}
	}
}

func (p *Pagination) HowManyPagesToSkip() int64 {
	return (p.Page - 1) * p.PerPage
}

func (p *Pagination) countTotalPages(totalDocs int64) int64 {
	t := totalDocs / p.PerPage
	if t == 0 && totalDocs > 0 {
		return 1
	}
	return t
}
