package main

import (
	"fmt"
	"github.com/caarlos0/env/v9"
)

type MongoCredentials struct {
	User string `env:"MONGO_DB_USER" envDefault:"root"`
	Pass string `env:"MONGO_DB_PASSWORD" envDefault:"example"`
	Host string `env:"MONGO_DB_HOST" envDefault:"localhost"`
	Port int    `env:"MONGO_DB_PORT" envDefault:"27017"`
}

func NewMongoCredentials() *MongoCredentials {
	c := &MongoCredentials{}
	if err := env.Parse(c); err != nil {
		panic(err)
	}

	return c
}

func (mc *MongoCredentials) GetURI() string {
	// "mongodb://<User>:<password>@<Host>:<Port>/" default Port=27017
	return fmt.Sprintf("mongodb://%s:%s@%s:%d", mc.User, mc.Pass, mc.Host, mc.Port)
}
