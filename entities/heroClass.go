package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

const HeroClassesCol = "heroClasses"

type HeroClass struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Name    string             `bson:"name,omitempty"`
	Attack  int                `bson:"attack,omitempty"`
	Defence int                `bson:"defence,omitempty"`
	Pierce  int                `bson:"pierce,omitempty"`
	HP      int                `bson:"HP,omitempty"`
}
