package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

const HeroAbilitiesCol = "heroAbilities"

type HeroAbility struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	Name        string             `bson:"name,omitempty"`
	Kind        string             `bson:"kind,omitempty"`
	Power       int                `bson:"power,omitempty"`
	PowerPerLvl int                `bson:"powerPerLvl,omitempty"`
}
