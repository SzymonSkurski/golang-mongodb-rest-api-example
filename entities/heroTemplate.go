package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// HeroTemplatesCol name of the collection
const HeroTemplatesCol = "heroTemplates"

type HeroTemplate struct {
	ID            primitive.ObjectID `bson:"_id,omitempty" json:"_id" form:"_id"`
	Name          string             `bson:"name,omitempty" json:"name" form:"name"`
	OptionalName2 *string            `bson:"optional_name_2" json:"optionalName2" form:"optionalName2"`
	Class         string             `bson:"class,omitempty" json:"class" form:"class"`
	Abilities     []string           `bson:"abilities,omitempty" json:"abilities" form:"abilities"`
}
