package entities

import "seeds"

type Entity struct {
	ColName  string
	SeedFunc []interface{}
}

func Entities() []Entity {
	return []Entity{
		{
			ColName:  HeroClassesCol,
			SeedFunc: seeds.HeroClassesSeed(),
		},
		{
			ColName:  HeroAbilitiesCol,
			SeedFunc: seeds.HeroClassesSeed(),
		},
		{
			ColName:  HeroTemplatesCol,
			SeedFunc: seeds.HeroClassesSeed(),
		},
	}
}
