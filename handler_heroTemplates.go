package main

import (
	entities "authentication"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
	"strings"
)

// heroTemplateFindOne CRUD Read
// will return one document by given name or id
func heroTemplateFindOne(app *APP) gin.HandlerFunc {
	return func(c *gin.Context) {
		// bind wildcard
		_, needle, _ := strings.Cut(c.Param("needle"), "/")

		repository := NewHeroTemplateRepository(app.MongoDB)

		// find by id
		if ht, err := repository.FindOneByID(needle); err == nil {
			c.JSON(http.StatusOK, ht)
			return
		}

		// find by name
		if ht, err := repository.FindOneByName(needle); err == nil {
			c.JSON(http.StatusOK, ht)
			return
		}

		// empty response
		c.JSON(http.StatusNoContent, gin.H{entities.HeroTemplatesCol: []entities.HeroTemplate{}})
	}
}

// heroTemplateFind CRUD Read
// except GET params as filters
// empty filters will return whole document
// example ?class=tank will return all with class:tank
// response is ordered by _id as default
// response order could be changed by sending order_by
// order_by?=<field1>:<value> or order_by?=<field1> (default 1)
// <value> accept: 1 ASCending (default), -1 DESCending
// example ?order_by=_id:-1 will order by _id DESCending
// example ?order_by=name will order by name ASCending (default 1)
func heroTemplateFind(app *APP) gin.HandlerFunc {
	return func(c *gin.Context) {
		// bind query params
		heroTemplate := entities.HeroTemplate{}
		_ = c.Bind(&heroTemplate)

		repository := NewHeroTemplateRepository(app.MongoDB)

		// bind paginate params
		pagination := NewPagination(c.Query(queryPaginate), repository.Count(heroTemplate))

		// bind order params
		order := BindOrderBy(c.Query(queryOrderBy))

		c.JSON(http.StatusOK, gin.H{
			entities.HeroTemplatesCol: repository.Multiple(heroTemplate, order, pagination),
			"pagination":              pagination,
		})
	}
}

// heroTemplateDelete CRUD Delete
// delete on document by given in query ID
func heroTemplateDelete(app *APP) gin.HandlerFunc {
	return func(c *gin.Context) {
		// bind wildcard
		_, ID, _ := strings.Cut(c.Param("ID"), "/")

		repository := NewHeroTemplateRepository(app.MongoDB)

		r, err := repository.Delete(ID)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"msg": r})
	}
}

func heroTemplateUpdate(app *APP) gin.HandlerFunc {
	return func(c *gin.Context) {
		// bind body json params
		heroTemplate := entities.HeroTemplate{}
		if err := c.ShouldBindBodyWith(&heroTemplate, binding.JSON); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		repository := NewHeroTemplateRepository(app.MongoDB)

		msg, err := repository.UpdateOne(heroTemplate)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"msg": msg})
	}
}

func heroTemplateCreate(app *APP) gin.HandlerFunc {
	return func(c *gin.Context) {
		// bind POST params
		heroTemplate := entities.HeroTemplate{}
		if err := c.ShouldBind(&heroTemplate); err != nil {
			//prettyprinter.PP(heroTemplate)
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		repository := NewHeroTemplateRepository(app.MongoDB)

		msg, err := repository.CreateOne(heroTemplate)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"msg": msg})
	}
}
