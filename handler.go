package main

import (
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"strconv"
	"strings"
)

// common functions & types for all handlers

const queryOrderBy = "order_by"
const queryPaginate = "paginate"

func ListRoutesHandler(app *APP) gin.HandlerFunc {
	return func(c *gin.Context) {
		var rDesc []RouteDesc
		for _, route := range routing() {
			rDesc = append(rDesc, route.Desc)
		}
		c.JSON(200, rDesc)
	}
}

func BindOrderBy(orderBy string) interface{} {
	if len(orderBy) == 0 {
		return nil
	}
	if os := strings.Split(orderBy, ":"); len(os) > 0 {
		o := ""
		if len(os) > 1 {
			o = os[1]
		}
		return bson.M{os[0]: getValidOrderByValue(o)}
	}
	return nil
}

func getValidOrderByValue(ob string) int {
	valid := [2]int{1, -1}
	def := valid[0] // ASCending
	obi, err := strconv.ParseInt(ob, 10, 64)
	if err != nil {
		return def // 1 default
	}
	for _, v := range valid {
		if v == int(obi) {
			return v
		}
	}
	return def
}
