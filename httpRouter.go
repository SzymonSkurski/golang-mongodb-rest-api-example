package main

import (
	entities "authentication"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
)

type RouteDesc struct {
	Method      string // GET, POST ...
	Path        string // relative path like users/*id
	Description string // information for API end user
}

type Route struct {
	Desc        RouteDesc
	HandlerFunc func(app *APP) gin.HandlerFunc // handler function
}

// routing will return full route table
func routing() []Route {
	return []Route{
		{
			Desc: RouteDesc{
				Method:      "GET",
				Path:        "/",
				Description: "List all API routes",
			},
			HandlerFunc: ListRoutesHandler,
		},
		{
			Desc: RouteDesc{
				Method: "GET",
				Path:   fmt.Sprintf("%s", entities.HeroTemplatesCol),
				Description: "Filter heroTemplates by get params, like ?name=Tank 1, empty filter will return all documents. " +
					"Paginate output by paginate param, like ?paginate=pp:25,p:1 will display page 1 with 25 documents per page",
			},
			HandlerFunc: heroTemplateFind,
		},
		{
			Desc: RouteDesc{
				Method:      "GET",
				Path:        fmt.Sprintf("%s/*needle", entities.HeroTemplatesCol),
				Description: "Get one heroTemplate by send name or id",
			},
			HandlerFunc: heroTemplateFindOne,
		},
		{
			Desc: RouteDesc{
				Method:      "DELETE",
				Path:        fmt.Sprintf("%s/*ID", entities.HeroTemplatesCol),
				Description: "Delete one heroTemplate by given _id",
			},
			HandlerFunc: heroTemplateDelete,
		},
		{
			Desc: RouteDesc{
				Method:      "PUT",
				Path:        fmt.Sprintf("%s", entities.HeroTemplatesCol),
				Description: "UpdateOne heroTemplate according to send JSON in request body, bind by _id",
			},
			HandlerFunc: heroTemplateUpdate,
		},
		{
			Desc: RouteDesc{
				Method:      "POST",
				Path:        fmt.Sprintf("%s", entities.HeroTemplatesCol),
				Description: "Crate heroTemplate according to send POST params",
			},
			HandlerFunc: heroTemplateCreate,
		},
	}
}

func declareRoutes(app *APP) {
	r := app.Engine
	for _, route := range routing() {
		switch route.Desc.Method {
		case "GET":
			r.GET(route.Desc.Path, route.HandlerFunc(app))
			break
		case "POST":
			r.POST(route.Desc.Path, route.HandlerFunc(app))
			break
		case "PUT":
			r.PUT(route.Desc.Path, route.HandlerFunc(app))
			break
		case "DELETE":
			r.DELETE(route.Desc.Path, route.HandlerFunc(app))
			break
		default:
			log.Fatal(fmt.Sprintf("unsupported route method: %s at route: %v", route.Desc.Method, route))
		}
	}
}
