package main

import (
	"github.com/caarlos0/env/v9"
)

type Config struct {
	Credentials *MongoCredentials
	Server      *ServerConfig
}

func NewConfig() *Config {

	return &Config{
		Credentials: NewMongoCredentials(),
		Server:      NewServerConfig(),
	}
}

type ServerConfig struct {
	Host string `env:"HOST" envDefault:"0.0.0.0"`
	Port int    `env:"PORT" envDefault:"8085"`
}

func NewServerConfig() *ServerConfig {
	cfg := &ServerConfig{}
	if err := env.Parse(cfg); err != nil {
		panic(err)
	}

	return cfg
}
