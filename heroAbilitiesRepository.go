package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
)

func heroAbilitiesAll(collection *mongo.Collection, ctx context.Context) interface{} {
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	abilities := &[]bson.M{}
	if err = cursor.All(ctx, abilities); err != nil {
		log.Fatal(err)
	}
	return abilities
}
