package main

import (
	"context"
	"github.com/gin-gonic/gin"
)

type APP struct {
	Engine  *gin.Engine
	MongoDB *MongoStorage
}

func NewAPP(cfg *Config) *APP {
	return &APP{
		Engine:  gin.Default(),
		MongoDB: NewMongoStorage(cfg.Credentials.GetURI(), context.Background(), func() {}),
	}
}
